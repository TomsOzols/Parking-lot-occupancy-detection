from os import path
import cv2
# from imutils.object_detection import non_max_suppression
import numpy
# import imutils

CARS_CASCADE = "/HaarXMLS/cars.xml"

def findCars(img, backgroundSubtractor):
    #resizedImage = imutils.resize(img, width=min(1024, img.shape[1]))
    #resizedImage = cv2.imread(pathToImage, cv2.IMREAD_COLOR)
    
    grayScale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #blurred = cv2.GaussianBlur(grayScale, (5,5), 0)

    mask = backgroundSubtractor.apply(grayScale)
    threshold = cv2.threshold(mask, 5, 255, cv2.THRESH_BINARY)[1]
    dilation = cv2.dilate(threshold, None, iterations = 2)

    im2, cnts, _ = cv2.findContours(dilation.copy(),
                                    cv2.RETR_EXTERNAL,
                                    cv2.CHAIN_APPROX_SIMPLE)

    minimumContourArea = 5000
    for c in cnts:
        if cv2.contourArea(c) < minimumContourArea:
            continue

        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(img, (x, y),(x+w, y+h),(0,255,0), 2)
    
    dirName = path.dirname(path.realpath(__file__))
    cascadeFilePath = dirName + CARS_CASCADE
    car_cascade = cv2.CascadeClassifier(cascadeFilePath)

    car = car_cascade.detectMultiScale(grayScale, scaleFactor=1.1, minNeighbors=3, maxSize=(100, 100))

    #rectsAdjusted = numpy.array([[x, y, x + w, y + h] for (x, y, w, h) in car])
    #suppressedCars = non_max_suppression(rectsAdjusted, probs=None, overlapThresh=0.9)
    
    for (x,y,w,h) in car:
        cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)

    return img