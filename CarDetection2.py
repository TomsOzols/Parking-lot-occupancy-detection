import sys
from Vision.DetectCars import findCars

from os import listdir
from os.path import isfile, join

from datetime import datetime

import cv2
import UploadToGDrive

INPUT_PICTURE_PATH = "./InputImages/"
OUTPUT_PICTURE_PATH = "./ResultImages/"


RELATIVE_INPUT_IMAGE_PATH = "InputImages/"
RELATIVE_OUTPUT_IMAGE = "ResultImages/"
RELATIVE_OUTPUT_NMS = "ResultImagesNMS/"

CURRENT_DATE_AND_TIME = str(datetime.now())
CURRENT_RUN_FOLDER_ID = None

def setup():
    DRIVE_FOLDER_ID = UploadToGDrive.getFileId('Parking-lot-occupancy')
    global CURRENT_RUN_FOLDER_ID
    CURRENT_RUN_FOLDER_ID = UploadToGDrive.createFolder(CURRENT_DATE_AND_TIME, DRIVE_FOLDER_ID)

def driveUpload(imageObject):
    image, fileName, filePath = imageObject
    UploadToGDrive.uploadFile(filePath, fileName, CURRENT_RUN_FOLDER_ID)

def getAllInputPicturePaths():
    return [f for f in listdir(INPUT_PICTURE_PATH) if isfile(join(INPUT_PICTURE_PATH, f))]

#setup()

def getImageByPath(path):
    return cv2.imread(path, cv2.IMREAD_COLOR)

def processImage(imageName, backgroundSubtractor):
    pathToImage = RELATIVE_INPUT_IMAGE_PATH + imageName
    image = getImageByPath(pathToImage)
    processedImage = findCars(image, backgroundSubtractor)
    return processedImage, imageName, pathToImage

def showInWindow(imageObject):
    image, name, filePath = imageObject
    cv2.imshow(name, image)
    cv2.waitKey(0)

def processImages():    
    paths = getAllInputPicturePaths()
    sortedList = sorted(paths)
    shorterList = sortedList[:15]

    backgroundSubtractor = cv2.createBackgroundSubtractorMOG2()

    processedImages = [processImage(x, backgroundSubtractor) for x in shorterList]
    [showInWindow(x) for x in processedImages]
    # [driveUpload(x) for x in processedImages]
    cv2.destroyAllWindows()

try:
    processImages()
except Exception as ex:
    print(ex)
