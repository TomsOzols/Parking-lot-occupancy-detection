from __future__ import print_function
import httplib2
import os
import sys

from apiclient import discovery
from apiclient.http import MediaFileUpload
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

SCOPES = 'https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Drive API Python Quickstart'


def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'drive-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def initiateService():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    return service

def createMeta(fileName, folderId=None):
    if folderId:
        return {
            'name': fileName,
            'parents': [ folderId ]
        }

    return { 'name': fileName }

def createFolderMeta(folderName, parentFolderId=None):
    meta = createMeta(folderName, parentFolderId)
    meta['mimeType'] = 'application/vnd.google-apps.folder'
    
    return meta

def getFileId(fileName):
    service = initiateService()

    fileSearchString = "name = '" + fileName + "'"
    
    request = service.files().list(q=fileSearchString)
    response = request.execute()
    
    file = response['files'][0]
    if not file:
        raise Exception('File not found with name: ' + fileName)

    return file.get('id')

def uploadFile(filePath, fileName, folderId=None):
    fileMeta = createMeta(fileName, folderId)
    service = initiateService()
    
    media = MediaFileUpload(filePath, mimetype='image/jpeg')
    file = service.files().create(body=fileMeta,
                                        media_body=media,
                                        fields='id').execute()

    return file.get('id')

def createFolder(folderName, parentFolderId=None):
    fileMeta = createFolderMeta(folderName, parentFolderId)
    service = initiateService()

    file = service.files().create(body = fileMeta,
                                  fields = 'id').execute()

    fileId = file.get('id')
    return fileId
