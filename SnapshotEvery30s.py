import sys
sys.path.insert(0, '/home/pi/Parking-lot-occupancy-detection/PiCamera')

import time

import CameraScripts as Camera

sleepTime = 30

while True:
    Camera.TakePicture()    
    time.sleep(sleepTime)
