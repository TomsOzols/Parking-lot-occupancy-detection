## TODO:
Put most of the stuff written here into an appropriate place.

## A list of things done in order to get this going:
### 1) Setting up Raspberry Pi
I am using 
Raspbian Jessie with pixel
2017-04-10 release

```shell
sudo apt-get update
sudo apt-get dist-upgrade
```
### 2) Setting up the camera module
```shell
sudo raspi-config
```
- Choose to enable Pi camera
Had a lot of problems with a camera casing, the camera kept
turning off when the casing is closed.

### 3) Installing libraries We're dependent on
```shell
sudo apt-get install python3-picamera
```

The process I've used to build OpenCV can be found here:
[Install OpenCV on raspberry pi3 Jessie](http://www.pyimagesearch.com/2016/04/18/install-guide-raspberry-pi-3-raspbian-jessie-opencv-3/)

We could probably skip the video package installation,
but for sake of completeness, I'm installing all the listed dependencies.

### 4) Work environment
Since the OpenCV tutorial introduces the use of virtual environments,
I've decided to check it out, and see the benefits/downfalls of doing so.
When working in terminal, use the following commands to get into the virtual environment:
```shell
source ~/.profile
workon cv
```

Where cv is the name of the virtual environment.

### 5) Mentions
Current cars.xml taken from
[This github page](https://github.com/shaanhk/New-GithubTest/blob/master/cars.xml)
After reading [this](http://www.geeksforgeeks.org/opencv-python-program-vehicle-detection-video-frame/) article

Trying out another haar cascade file for car detection:
[Github](https://github.com/andrewssobral/vehicle_detection_haarcascades/blob/master/cars.xml)

Although they might be the same. Sticking to the first one for the time being, and leaving the other github page just for reference
Maybe as an idea, here is a [link](https://abhishek4273.com/2014/03/16/traincascade-and-car-detection-using-opencv/) to training Your own Haar cascades

Documentation for the detectMultiScale function, in several resources
[Here](http://docs.opencv.org/trunk/d1/de5/classcv_1_1CascadeClassifier.html)
[There](http://docs.opencv.org/java/2.4.9/org/opencv/objdetect/CascadeClassifier.html)
[And a github example](https://github.com/npinto/opencv/blob/master/samples/python2/facedetect.py)

### 6) Development dependencies
I'm intending to use a google drive api in order to upload input/result pictures - first of all in order to even further
detach the development away from the Pi, in order to keep it as an executing machine. (Remote desktop tends to be really slow when viewing images).

Google drive api client library installation
```shell
sudo pip install--upgrade google-api-python-client
```

#### The api username:
Parking-project
#### Client id:
323366949938-ieqjgqtbve9u0s3sd7a04ej1mtkckhku.apps.googleusercontent.com

Included in the repository should be a json configuration file containing connection details.
[Tutorial for the api](https://developers.google.com/drive/v3/web/quickstart/python)