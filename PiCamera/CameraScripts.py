import time
from datetime import datetime
import picamera

def TakePicture():
    CAMERA = picamera.PiCamera()
    CURRENT_TIME = datetime.now()
    READABLE_TIME = str(CURRENT_TIME)

    FILE_NAME = "pictures/capture-" + READABLE_TIME + ".jpg"

    CAMERA.capture(FILE_NAME)
    CAMERA.close()
    return

def StartPreview(previewTime):
    CAMERA = picamera.PiCamera()
    CAMERA.start_preview()

    time.sleep(previewTime)

    CAMERA.stop_preview()
    return
