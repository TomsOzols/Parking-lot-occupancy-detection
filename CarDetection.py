from os import listdir
from os.path import isfile, join
from imutils.object_detection import non_max_suppression
from datetime import datetime
import numpy
import imutils
import cv2
import UploadToGDrive

INPUT_PICTURE_PATH = "/home/pi/Parking-lot-occupancy-detection/InputImages/"
OUTPUT_PICTURE_PATH = "/home/pi/Parking-lot-occupancy-detection/ResultImages/"
CARS_CASCADE = "/home/pi/Parking-lot-occupancy-detection/HaarXMLS/cars.xml"

RELATIVE_INPUT_IMAGE_PATH = "InputImages/"
RELATIVE_OUTPUT_IMAGE = "ResultImages/"
RELATIVE_OUTPUT_NMS = "ResultImagesNMS/"

CURRENT_DATE_AND_TIME = str(datetime.now())
CURRENT_RUN_FOLDER_ID = None

def setup():
    DRIVE_FOLDER_ID = UploadToGDrive.getFileId('Parking-lot-occupancy')
    global CURRENT_RUN_FOLDER_ID
    CURRENT_RUN_FOLDER_ID = UploadToGDrive.createFolder(CURRENT_DATE_AND_TIME, DRIVE_FOLDER_ID)

def driveUpload(filePath, fileName):
    UploadToGDrive.uploadFile(filePath, fileName, CURRENT_RUN_FOLDER_ID)

def getAllInputPicturePaths():
    return [f for f in listdir(INPUT_PICTURE_PATH) if isfile(join(INPUT_PICTURE_PATH, f))]

def findCars(imageName):
    pathToImage = RELATIVE_INPUT_IMAGE_PATH + imageName

    img = cv2.imread(pathToImage, cv2.IMREAD_COLOR)
    resizedImage = imutils.resize(img, width=min(1024, img.shape[1]))
    #resizedImage = cv2.imread(pathToImage, cv2.IMREAD_COLOR)
    
    grayScale = cv2.cvtColor(resizedImage, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(grayScale, (5,5), 0)
    car_cascade = cv2.CascadeClassifier(CARS_CASCADE)

    car = car_cascade.detectMultiScale(blurred, scaleFactor=1.01, minNeighbors=3, maxSize=(100, 100))

    #rectsAdjusted = numpy.array([[x, y, x + w, y + h] for (x, y, w, h) in car])
    #suppressedCars = non_max_suppression(rectsAdjusted, probs=None, overlapThresh=0.9)
    
    for (x,y,w,h) in car:
        cv2.rectangle(resizedImage,(x,y),(x+w,y+h),(0,0,255),2)

    resultImagePath = RELATIVE_OUTPUT_IMAGE + imageName
    cv2.imwrite(resultImagePath, resizedImage)

    driveUpload(resultImagePath, imageName)
    
    #cv2.imshow("Result", resizedImage)
    #cv2.waitKey(0)

setup()
[findCars(x) for x in getAllInputPicturePaths()]

cv2.destroyAllWindows()
